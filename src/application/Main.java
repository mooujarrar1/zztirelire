package application;
	
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.time.Clock;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import com.pi4j.io.gpio.trigger.GpioCallbackTrigger;
import com.sun.javafx.scene.control.skin.FXVK;

import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.PauseTransition;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.effect.Glow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

<<<<<<< HEAD
import java.util.concurrent.Callable;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.trigger.GpioCallbackTrigger;
import com.pi4j.io.gpio.trigger.GpioPulseStateTrigger;
import com.pi4j.io.gpio.trigger.GpioSetStateTrigger;
import com.pi4j.io.gpio.trigger.GpioSyncStateTrigger;
=======
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

>>>>>>> c4206eb7138a6b5cec416eb3af092c0c1a25203f

public class Main extends Application  {


	Image image;
	static String PSK;
	static String SSID;
	static Scene scene;
    static Socket socket;
    static Label capteur;
	static Image image2 = null;
	static ImageView imView2 = null;
	
	@SuppressWarnings("null")
	@Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("ZZTirelire");
        final GpioController gpio = GpioFactory.getInstance();

        final GpioPinDigitalInput myButton = gpio.provisionDigitalInputPin(RaspiPin.GPIO_23,
                PinPullResistance.PULL_DOWN);
       
        
        BorderPane borderpane = new BorderPane();
        
        
        borderpane.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
        
        capteur = new Label();
        capteur.setText("0");
        
        DigitalClock montre = new DigitalClock();
        montre.setId("rich-blue");
        
    	GridPane grid = new GridPane();
    	grid.setAlignment(Pos.CENTER);
    	grid.setHgap(3);
    	grid.setVgap(3);
    	
    	Label userName = new Label("SSID:");
    	userName.setTextFill(Color.web("#6b94e0"));
    	grid.add(userName, 0, 1);
    	
    	TextField userTextField = new TextField();
    	grid.add(userTextField, 1, 1);

    	Label pw = new Label("Cl�:");
    	pw.setTextFill(Color.web("#6b94e0"));
    	grid.add(pw, 0, 2);

    	PasswordField pwBox = new PasswordField();
    	grid.add(pwBox, 1, 2);
        
        Button button1 = new Button("Connexion");
        Button button2 = new Button("Jeux");
        Button button3 = new Button("Tirelire");
        button1.setId("Button");
        button2.setId("Button"); 
        button3.setId("Button");
        VBox vbox = new VBox(0);
    	
        
        
		HBox hbox = new HBox(5);

        HBox.setHgrow(button1, Priority.ALWAYS);
        HBox.setHgrow(button2, Priority.ALWAYS);
        HBox.setHgrow(button3, Priority.ALWAYS);
        button1.setMaxWidth(Double.MAX_VALUE);
        button2.setMaxWidth(Double.MAX_VALUE);
        button3.setMaxWidth(Double.MAX_VALUE);
        
        Parent layout = createLayout();
        layout.getStylesheets().add(getClass().getResource("clock.css").toExternalForm());
        
        montre.textProperty().addListener(new ChangeListener<String>() {
        
			@Override
			public void changed(ObservableValue<? extends String> arg0, String Old, String New) {
				Platform.runLater(()->{
			        if(checkInternetConnection().substring(0,1).equals("C")) {
			        	button1.setText("D�connexion");
			        	try {
			        		vbox.getChildren().addAll(layout, montre);
			        	}
			        	catch(Exception e) {}
			        	try {
			        		hbox.getChildren().addAll(button2, button3);
			        	}
			        	catch(Exception e) {}
			        	try {
			        		vbox.getChildren().remove(grid);
			        	}
			        	catch(Exception e) {}
		        		
			        }else {
			        	button1.setText("Connexion");
			        	try {
			        		hbox.getChildren().removeAll(button2, button3);
			        		
			        	}
			        	catch(Exception e) {}
			        	try {
			        		vbox.getChildren().removeAll(layout, montre);
			        	}
			        	catch(Exception e) {}
			        	try {
			        		vbox.getChildren().add(grid);
			        	}
			        	catch(Exception e) {}
			        	
			        }
			        try {
					if(Integer.parseInt(New.substring(0, 2))>=19 && Integer.parseInt(Old.substring(0, 2))<19) {
						image = new Image(getClass().getResource("Background3.gif").toExternalForm());
				        
				        BackgroundImage myBI= new BackgroundImage(image,
				                BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, false, false));
				        
				        
				        
				        borderpane.setBackground(new Background(myBI));
					}else if(Integer.parseInt(New.substring(0, 2))<19 && Integer.parseInt(Old.substring(0, 2))>=19) {
	
						image = new Image(getClass().getResource("Background2.gif").toExternalForm());
				        
				        BackgroundImage myBI= new BackgroundImage(image,
				                BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, false, false));
	
				        borderpane.setBackground(new Background(myBI));
					}} catch(Exception e) {}
					
				});	
			}});

     
        
        Calendar rightNow = Calendar.getInstance();
        int heure = rightNow.get(Calendar.HOUR_OF_DAY);
		if(heure>=19) {
			image = new Image(getClass().getResource("Background3.gif").toExternalForm());
	        
	        BackgroundImage myBI= new BackgroundImage(image,
	                BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, false, false));
	        
	        
	        
	        borderpane.setBackground(new Background(myBI));
		}else{

			image = new Image(getClass().getResource("Background2.gif").toExternalForm());
	        
	        BackgroundImage myBI= new BackgroundImage(image,
	                BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, false, false));

	        borderpane.setBackground(new Background(myBI));
		}

        montre.setAlignment( Pos.CENTER_LEFT);
        
       

        
        vbox.getChildren().add(layout);
        vbox.getChildren().add(montre);
       
        if(checkInternetConnection().substring(0,1).equals("C")) {
        	button1.setText("D�connexion");

        }else {
        	hbox.getChildren().removeAll(button2,button3);
        	vbox.getChildren().removeAll(layout, montre);
        	vbox.getChildren().add(grid);
        }
        

        

       
        
        Label err = new Label("Authentification �chou�e");
        err.setTextFill(Color.web("#e01414")); 
        
        
        userTextField.getProperties().put("vkType", "email");
        pwBox.getProperties().put("vkType", "email");
      

        button1.setOnAction(new EventHandler<ActionEvent>() {
            @SuppressWarnings("deprecation")
			@Override
            public void handle(ActionEvent event) {

                if(button1.getText().equals("D�connexion")) {
                	borderpane.setCenter(null);
                	borderpane.setBottom(null);
                	
                    Button button4 = new Button("Oui");
                    Button button5 = new Button("Non");
                    button4.setId("Button");
                    button5.setId("Button"); 
                	
            		HBox hboxDeconnexion = new HBox(5);
                    HBox.setHgrow(button4, Priority.ALWAYS);
                    HBox.setHgrow(button5, Priority.ALWAYS);
                    button4.setMaxWidth(Double.MAX_VALUE);
                    button5.setMaxWidth(Double.MAX_VALUE);
                    
                    Label confirmation = new Label("Confirmez la d�connexion du r�seau");
                    confirmation.setId("rich-blue");
            		
                    hboxDeconnexion.getChildren().addAll(button4, button5);
                    hboxDeconnexion.setAlignment(Pos.CENTER_LEFT);
                    
                    borderpane.setCenter(confirmation);
                    borderpane.setBottom(hboxDeconnexion);
                    
                    scene.setRoot(borderpane);
                    
                    button4.setOnAction(new EventHandler<ActionEvent>() {
                        @SuppressWarnings("deprecation")
            			@Override
                        public void handle(ActionEvent event) { 
                        	try {
                    			grid.getChildren().remove(err);
                    		} catch(Exception e) {
                    			
                    		}
                    		userTextField.clear();
                    		pwBox.clear();
                    		hbox.getChildren().removeAll(button2, button3);
                    		Platform.runLater(new Runnable() {
        					    @Override
        					    public void run() {
        					    	vbox.getChildren().removeAll(layout, montre);
        					    	vbox.getChildren().add(grid);
        					    	File myFoo = new File("/etc/wpa_supplicant/wpa_supplicant.conf");
        				        	FileWriter fooWriter;
        							try {
        								fooWriter = new FileWriter(myFoo, false);
        						    	fooWriter.write("ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\n");
        						    	fooWriter.write("update_config=1\n");
        						    	fooWriter.write("country=FR\n");
        						    	
        						    	fooWriter.close();
        						    	
        							} catch (IOException e2) {
        								// TODO Auto-generated catch block
        								e2.printStackTrace();
        							} // true to append
        				        	
        				        	Process proc = null;
        				        	
        				            try {
        				                proc = Runtime.getRuntime().exec("sh ./refresh.sh");
        				            } catch (IOException e) {
        				                // TODO Auto-generated catch block
        				                e.printStackTrace();
        				            }
                                	
        					    }
        					});

                            borderpane.setCenter(vbox);
                            borderpane.setBottom(hbox);
                            
                            scene.setRoot(borderpane);
                        }});
                    button5.setOnAction(new EventHandler<ActionEvent>() {
                        @SuppressWarnings("deprecation")
            			@Override
                        public void handle(ActionEvent event) { 
                       
                            borderpane.setCenter(vbox);
                            borderpane.setBottom(hbox);
                            
                            scene.setRoot(borderpane);
                            
                        }});
                	
                	
                }else {          		
                	Platform.runLater(new Runnable() {
                	    @Override
                	    public void run() {
                	    	try {
                	    		grid.getChildren().remove(err);
                	    	}catch(Exception e) {}
                	    	vbox.getChildren().remove(grid);
                        	Main.SSID = userTextField.getText();
                        	Main.PSK = pwBox.getText();
                	    	File myFoo = new File("/etc/wpa_supplicant/wpa_supplicant.conf");
                        	
                        	try {
                        		FileWriter fooWriter = new FileWriter(myFoo, false); // true to append
                                // false to overwrite.
                				fooWriter.write("ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\n");
                				fooWriter.write("update_config=1\n");
                            	fooWriter.write("country=FR\n");
                            	fooWriter.write("network={\n");
                            	fooWriter.write("\tssid=\""+Main.SSID+"\"\n");
                            	if(Main.PSK.equals("")) {
                            		fooWriter.write("\tkey_mgmt=NONE\n");
                            	}
                            	else {
                            		fooWriter.write("\tpsk=\""+PSK+"\"\n");
                            		fooWriter.write("\tkey_mgmt=WPA-PSK\n");
                            	}
                            	fooWriter.write("}\n");
                            	fooWriter.close();
                            	
                			} catch (IOException e1) {
                				// TODO Auto-generated catch block
                				e1.printStackTrace();
                			}
                        	
                        	Process proc = null;
                        	
                            try {
                                proc = Runtime.getRuntime().exec("sh ./refresh.sh");
                            } catch (IOException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            

                            
                        	try {
								Thread.sleep(10000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
                        	if(!checkInternetConnection().substring(0, 1).equals("C")) {
        						try {
        							grid.add(err, 1, 3);
        						}catch(Exception e) {
        							
        						} try {
        							vbox.getChildren().removeAll(layout, montre);
        						} catch(Exception e) {}
        						vbox.getChildren().add(grid);
        					}else {
        						vbox.getChildren().addAll(layout, montre);
        					}
                        	
                	    }
                	});

                	
                	
                }
            }
        });
        
        Button button4 = new Button("Detail");
        button4.setId("Button");
        
        HBox hboxDetail = new HBox(5);
        VBox vboxBillets = new VBox(2);
        VBox vboxPieces = new VBox(10);
        
		HBox hboxBottom = new HBox(5);
		HBox hboxTop = new HBox(5);
		
        Label montant = new Label("Le montant dans la tirelire est :"+ readFile("montant", StandardCharsets.UTF_8));
        montant.setId("rich-blue");
        
        Image image = new Image(getClass().getResource("back.gif").toExternalForm());
        ImageView imView = new ImageView();
        imView.setImage(image);
        imView.setFitWidth(30);
        imView.setFitHeight(30);
		
    	hboxTop.getChildren().add(imView);
    	hboxTop.setAlignment(Pos.CENTER_LEFT);
    	

		hboxBottom.setHgrow(button4, Priority.ALWAYS);
        button4.setMaxWidth(Double.MAX_VALUE);
        

        hboxBottom.getChildren().addAll(button4);//button5
        hboxBottom.setAlignment(Pos.CENTER_LEFT);
    	
        button3.setOnAction(new EventHandler<ActionEvent>() {
            @SuppressWarnings("deprecation")
			@Override
            public void handle(ActionEvent event) {
                	borderpane.setCenter(null);
                	borderpane.setBottom(null);

                    borderpane.setTop(hboxTop);
                    borderpane.setCenter(montant);
                    borderpane.setBottom(hboxBottom);
                    
                    scene.setRoot(borderpane);
                }
        });
        
<<<<<<< HEAD
        
        System.out.println("<--Pi4J--> GPIO Trigger Example ... started.");

        // create gpio controller
        final GpioController gpio = GpioFactory.getInstance();

        // provision gpio pin #02 as an input pin with its internal pull down resistor enabled
        final GpioPinDigitalInput myButton = gpio.provisionDigitalInputPin(RaspiPin.GPIO_06,
                                                  PinPullResistance.PULL_DOWN);

        // create a gpio callback trigger on gpio pin#4; when #4 changes state, perform a callback
        // invocation on the user defined 'Callable' class instance
        myButton.addTrigger(new GpioCallbackTrigger(new Callable<Void>() {
            public Void call() throws Exception {
                System.out.println(" --> GPIO TRIGGER CALLBACK RECEIVED ");
                return null;
            }
        }));
        
=======
        imView.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
            	if(borderpane.getChildren().contains(hboxBottom)) {
            		borderpane.setTop(null);
                  	borderpane.setCenter(null);
                  	borderpane.setBottom(null);
                    borderpane.setCenter(vbox);
                    borderpane.setBottom(hbox);
            	}else if(borderpane.getChildren().contains(vboxBillets) || borderpane.getChildren().contains(vboxPieces)) {
            		borderpane.setBottom(null);
                  	borderpane.setCenter(null);
                  	borderpane.setTop(null);
                    borderpane.setTop(hboxTop);
                    borderpane.setCenter(montant);
                    borderpane.setBottom(hboxDetail);
            	}
            	else {
            		borderpane.setBottom(null);
                    borderpane.setBottom(hboxBottom);
            	}
                scene.setRoot(borderpane);
            }
       });
        
        Button button7 = new Button("Detail pi�ces");
        button7.setId("Button");
        Button button8 = new Button("Detail billets");
        button8.setId("Button");
        
        button4.setOnAction(new EventHandler<ActionEvent>() {
        	@SuppressWarnings("deprecation")
        	@Override
            public void handle(ActionEvent event) {
        		borderpane.setBottom(null);
        		
        		
        		
                
                if(hboxDetail.getChildren().size()==0) {
                	hboxDetail.getChildren().addAll(button7,button8);
                	hboxDetail.setAlignment(Pos.CENTER_LEFT);
                	hboxDetail.setHgrow(button7, Priority.ALWAYS);
                    hboxDetail.setHgrow(button8, Priority.ALWAYS);
                    button7.setMaxWidth(Double.MAX_VALUE);
                    button8.setMaxWidth(Double.MAX_VALUE);
                }
                
                
                
                borderpane.setBottom(hboxDetail);
                
                scene.setRoot(borderpane);

        	}
        });
        
        
		
        Label quantite_1_piece = new Label(readFile("quantite_2", StandardCharsets.UTF_8)+" x ");
        quantite_1_piece.setId("rich-blue");
        
        Label quantite_2_piece = new Label(readFile("quantite_1", StandardCharsets.UTF_8)+" x ");
        quantite_2_piece.setId("rich-blue");
		
        Label quantite_3_piece = new Label(readFile("quantite_05", StandardCharsets.UTF_8)+" x ");
        quantite_3_piece.setId("rich-blue");
        
		Image coin1 = new Image(getClass().getResource("2euros.gif").toExternalForm());
        ImageView Euros_2 = new ImageView();
        Euros_2.setImage(coin1);
        Euros_2.setFitWidth(45);
        Euros_2.setFitHeight(45);
        
		Image coin2 = new Image(getClass().getResource("1euro.gif").toExternalForm());
        ImageView Euros_1 = new ImageView();
        Euros_1.setImage(coin2);
        Euros_1.setFitWidth(56);
        Euros_1.setFitHeight(52);
        
		Image coin3 = new Image(getClass().getResource("50cent.gif").toExternalForm());
        ImageView Cent_50 = new ImageView();
        Cent_50.setImage(coin3);
        Cent_50.setFitWidth(40);
        Cent_50.setFitHeight(40);
        
        HBox ligne1_pieces = new HBox(5);
        ligne1_pieces.setHgrow(quantite_1_piece, Priority.ALWAYS);
        ligne1_pieces.setHgrow(Euros_2, Priority.ALWAYS);
        quantite_1_piece.setAlignment(Pos.CENTER);
        ligne1_pieces.getChildren().addAll(quantite_1_piece,Euros_2);
        HBox ligne2_pieces = new HBox(5);
        ligne2_pieces.setHgrow(quantite_2_piece, Priority.ALWAYS);
        ligne2_pieces.setHgrow(Euros_1, Priority.ALWAYS);
        quantite_2_piece.setAlignment(Pos.CENTER);
        ligne2_pieces.getChildren().addAll(quantite_2_piece,Euros_1);
        HBox ligne3_pieces = new HBox(5);
        ligne3_pieces.setHgrow(quantite_3_piece, Priority.ALWAYS);
        ligne3_pieces.setHgrow(Cent_50, Priority.ALWAYS);
        quantite_3_piece.setAlignment(Pos.CENTER);
        ligne3_pieces.getChildren().addAll(quantite_3_piece,Cent_50);
        
        ligne1_pieces.setAlignment(Pos.CENTER);
        ligne2_pieces.setAlignment(Pos.CENTER);
        ligne3_pieces.setAlignment(Pos.CENTER);
        
        button7.setOnAction(new EventHandler<ActionEvent>() {
        	@SuppressWarnings("deprecation")
        	@Override
            public void handle(ActionEvent event) {
        		borderpane.setBottom(null);
        		borderpane.setCenter(null);
        		

                if(vboxPieces.getChildren().size() == 0) {
                	vboxPieces.getChildren().addAll(ligne1_pieces, ligne2_pieces, ligne3_pieces);
            		vboxPieces.setAlignment(Pos.CENTER);
                }
        		borderpane.setCenter(vboxPieces);
        		scene.setRoot(borderpane);
        		
        		
        	}
        });
        
		Label quantite_1_billet = new Label(readFile("quantite_5", StandardCharsets.UTF_8)+" x ");
		quantite_1_billet.setId("shiny-orange");
        
        Label quantite_2_billet = new Label(readFile("quantite_10", StandardCharsets.UTF_8)+" x ");
        quantite_2_billet.setId("shiny-orange");
		
        Label quantite_3_billet = new Label(readFile("quantite_20", StandardCharsets.UTF_8)+" x ");
        quantite_3_billet.setId("shiny-orange");
        
        Label quantite_4_billet = new Label(readFile("quantite_50", StandardCharsets.UTF_8)+" x ");
        quantite_4_billet.setId("shiny-orange");
        
		Image bill_5 = new Image(getClass().getResource("bill_5.gif").toExternalForm());
        ImageView Euros_5 = new ImageView();
        Euros_5.setImage(bill_5);
        Euros_5.setFitWidth(90);
        Euros_5.setFitHeight(60);
        
		Image bill_10 = new Image(getClass().getResource("bill_10.gif").toExternalForm());
        ImageView Euros_10 = new ImageView();
        Euros_10.setImage(bill_10);
        Euros_10.setFitWidth(90);
        Euros_10.setFitHeight(60);
        
		Image bill_20 = new Image(getClass().getResource("bill_20.gif").toExternalForm());
        ImageView Euros_20 = new ImageView();
        Euros_20.setImage(bill_20);
        Euros_20.setFitWidth(80);
        Euros_20.setFitHeight(40);
        
		Image bill_50 = new Image(getClass().getResource("bill_50.gif").toExternalForm());
        ImageView Euros_50 = new ImageView();
        Euros_50.setImage(bill_50);
        Euros_50.setFitWidth(80);
        Euros_50.setFitHeight(40);
        
        
        HBox ligne1_billet = new HBox(5);
        ligne1_billet.setHgrow(quantite_1_billet, Priority.ALWAYS);
        ligne1_billet.setHgrow(Euros_5, Priority.ALWAYS);
        quantite_1_billet.setAlignment(Pos.CENTER);
        ligne1_billet.getChildren().addAll(quantite_1_billet,Euros_5);
        HBox ligne2_billet = new HBox(5);
        ligne2_billet.setHgrow(quantite_2_billet, Priority.ALWAYS);
        ligne2_billet.setHgrow(Euros_10, Priority.ALWAYS);
        quantite_2_billet.setAlignment(Pos.CENTER);
        ligne2_billet.getChildren().addAll(quantite_2_billet,Euros_10);
        HBox ligne3_billet = new HBox(5);
        ligne3_billet.setHgrow(quantite_3_billet, Priority.ALWAYS);
        ligne3_billet.setHgrow(Euros_20, Priority.ALWAYS);
        quantite_3_billet.setAlignment(Pos.CENTER);
        ligne3_billet.getChildren().addAll(quantite_3_billet,Euros_20);
        HBox ligne4_billet = new HBox(5);
        ligne4_billet.setHgrow(quantite_4_billet, Priority.ALWAYS);
        ligne4_billet.setHgrow(Euros_50, Priority.ALWAYS);
        quantite_4_billet.setAlignment(Pos.CENTER);
        ligne4_billet.getChildren().addAll(quantite_4_billet,Euros_50);
        
        ligne1_billet.setAlignment(Pos.CENTER);
        ligne2_billet.setAlignment(Pos.CENTER);
        ligne3_billet.setAlignment(Pos.CENTER);
        ligne4_billet.setAlignment(Pos.CENTER);
        
        button8.setOnAction(new EventHandler<ActionEvent>() {
        	@SuppressWarnings("deprecation")
        	@Override
            public void handle(ActionEvent event) {
        		borderpane.setBottom(null);
        		borderpane.setCenter(null);
        		

                if(vboxBillets.getChildren().size() == 0) {
                	vboxBillets.getChildren().addAll(ligne1_billet, ligne2_billet, ligne3_billet, ligne4_billet);
                	vboxBillets.setAlignment(Pos.CENTER);
                }
        		borderpane.setCenter(vboxBillets);
        		scene.setRoot(borderpane);
        	}
        });
        
        
        Image image1 = new Image(getClass().getResource("insertcoin.gif").toExternalForm());
        ImageView imView1 = new ImageView(image1);
        imView1.setFitWidth(130);
        imView1.setFitHeight(110);
        
        new Thread(() -> {
        	
        	try {
            	  socket = new Socket("localhost",9999);

            	  // Create an input stream to receive data from the server
            	  BufferedReader stdIn =new BufferedReader(new InputStreamReader(socket.getInputStream()));
                  PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            	  try{
            	        while(true){  	
                            String in = stdIn.readLine();
                            Platform.runLater(() -> {
                            	boolean piece = false;
                                System.out.println(in);
                                capteur.setText(in);
                                out.print("Try...");
                                out.flush();
                                FileWriter fr = null;
                                FileWriter fr1 = null;
                                double quantite = 0;
        						
                                
                                if(Double.parseDouble(in) == 0.5) {
                                	//affichage Piece de 0.5 euro ajout�e
                                	double Montant = Double.parseDouble(readFile("montant", StandardCharsets.UTF_8));
                                	try {
                                			quantite =  Double.parseDouble(readFile("quantite_05", StandardCharsets.UTF_8));
                                	    	fr = new FileWriter(new File("montant"), false);
                                	        fr.write(Double.toString(Montant+0.5));
                                	        fr.write("\r\n");
                                	        fr1 = new FileWriter(new File("quantite_05"), false);
                                	        fr1.write(Double.toString(quantite+1));
                                	} catch (Exception e) {
                                	  System.out.println("Error al escribir");
                                	} finally {
	                                	try {
	        								fr.close();
	        								fr1.close();
	        							} catch (IOException e) {
	        								// TODO Auto-generated catch block
	        								e.printStackTrace();
	        							}
                                	}
                                	piece = true;
                                	image2 = new Image(getClass().getResource("50centinsere.gif").toExternalForm());
                                }else if(Double.parseDouble(in) == 1) {
                                	//affichage Piece de 1 euro ajout�e
                                	double Montant = Double.parseDouble(readFile("montant", StandardCharsets.UTF_8));                                	
                                	try {
                                			quantite = Double.parseDouble(readFile("quantite_1", StandardCharsets.UTF_8));
                                	    	fr = new FileWriter(new File("montant"), false);
                                	    	System.out.println("montant actuel = "+Double.toString(Montant+1.0));
                                	        fr.write(Double.toString(Montant+1.0));
                                	        fr.write("\r\n");
                                	        fr1 = new FileWriter(new File("quantite_1"), false);
                                	        fr1.write(Double.toString(quantite+1));
                                	} catch (Exception e) {
                                	  System.out.println("Error al escribir");
                                	} finally {
	                                	try {
	        								fr.close();
	        								fr1.close();
	        							} catch (IOException e) {
	        								// TODO Auto-generated catch block
	        								e.printStackTrace();
	        							}
                                	}
                                	piece = true;
                                	image2 = new Image(getClass().getResource("2Euro_insere.gif").toExternalForm());
                                }
                                else if(Double.parseDouble(in) == 2) {
                                	//affichage Piece de 2 euro ajout�e
                                	double Montant = Double.parseDouble(readFile("montant", StandardCharsets.UTF_8));
                                	
                                	try {
                                			quantite =  Double.parseDouble(readFile("quantite_2", StandardCharsets.UTF_8));
                                	    	fr = new FileWriter(new File("montant"), false);
                                	        fr.write(Double.toString(Montant+2.0));
                                	        fr.write("\r\n");
                                	        fr1 = new FileWriter(new File("quantite_2"), false);
                                	        fr1.write(Double.toString(quantite+1));
                                	} catch (Exception e) {
                                	  System.out.println("Error al escribir");
                                	} finally {
                                	    try {
        								   fr.close();
        								   fr.close();
	        							} catch (IOException e) {
	        								// TODO Auto-generated catch block
	        								e.printStackTrace();
	        							}
                                	}
                                	piece = true;
                                	image2 = new Image(getClass().getResource("1eurounsere.gif").toExternalForm());
                                    
                                }
                                imView2 = new ImageView(image2);
                                imView2.setFitWidth(250);
                                imView2.setFitHeight(60);
                                try {
	                                borderpane.setCenter(null);
	                           	 	borderpane.setBottom(null);
	                           	 	borderpane.setTop(null);
                                }catch(Exception n) {
                                	n.printStackTrace();
                                }
                           	 	borderpane.setCenter(imView1);
                           	 	borderpane.setBottom(imView2);
                           	 	borderpane.setAlignment(imView2, Pos.CENTER);
                                scene.setRoot(borderpane);
                                
                                PauseTransition delay = new PauseTransition(Duration.seconds(5));
                                delay.setOnFinished( event -> {
                                	
                                      montant.setText("Le montant dans la tirelire est :"+ readFile("montant", StandardCharsets.UTF_8));

                                      borderpane.setTop(hboxTop);
                                      borderpane.setCenter(montant);
                                      borderpane.setBottom(hboxBottom);
                                      
                                      scene.setRoot(borderpane);
                                });
                                delay.play();

                           });
            	        }
            	    }
            	    catch(IOException e){
            	    	e.printStackTrace();
            	    }finally {
            	    	out.close();
            	    }

            	}
            	catch (IOException ex) {
            		ex.printStackTrace();
            	}
        }).start();
        
   	 ChoiceBox cb1 = new ChoiceBox(FXCollections.observableArrayList(
    		    "0","1", "2", "3", "4", "5")
    		);
    	ChoiceBox cb2 = new ChoiceBox(FXCollections.observableArrayList(
    			"0","1", "2", "3", "4", "5")
    		);
    	ChoiceBox cb3 = new ChoiceBox(FXCollections.observableArrayList(
    			"0","1", "2", "3", "4", "5")
    		);
    	ChoiceBox cb4 = new ChoiceBox(FXCollections.observableArrayList(
    			"0","1", "2", "3", "4", "5")
    		);
    	 	
     cb1.getSelectionModel().selectFirst();
     cb2.getSelectionModel().selectFirst();
     cb3.getSelectionModel().selectFirst();
     cb4.getSelectionModel().selectFirst();
     
      VBox vboxAjoutBillets = new VBox(2);
      HBox ligne1_ = new HBox(5);
        
      ligne1_.setHgrow(Euros_5, Priority.ALWAYS);
      ligne1_.setHgrow(cb1, Priority.ALWAYS);
      ligne1_.getChildren().addAll(Euros_5,cb1);
      HBox ligne2_ = new HBox(5);
      ligne2_.setHgrow(cb2, Priority.ALWAYS);
      ligne2_.setHgrow(Euros_10, Priority.ALWAYS);
      ligne2_.getChildren().addAll(Euros_10,cb2);
      HBox ligne3_ = new HBox(5);
      ligne3_.setHgrow(cb3, Priority.ALWAYS);
      ligne3_.setHgrow(Euros_20, Priority.ALWAYS);
      ligne3_.getChildren().addAll(Euros_20,cb3);
      HBox ligne4_ = new HBox(5);
      ligne4_.setHgrow(cb4, Priority.ALWAYS);
      ligne4_.setHgrow(Euros_50, Priority.ALWAYS);
      ligne4_.getChildren().addAll(Euros_50,cb4);
      
      ligne1_.setAlignment(Pos.CENTER);
      ligne2_.setAlignment(Pos.CENTER);
      ligne3_.setAlignment(Pos.CENTER);
      ligne4_.setAlignment(Pos.CENTER);
      vboxAjoutBillets.getChildren().addAll(ligne1_,ligne2_,ligne3_,ligne4_);
      
      HBox bouttonsAjoutbillet =  new HBox(5);
      
		Button buttonValider = new Button("Valider");
		buttonValider.setId("Button");
     Button buttonQuitter = new Button("Quitter");
     buttonQuitter.setId("Button");
     
     bouttonsAjoutbillet.getChildren().addAll(buttonValider,buttonQuitter);
     bouttonsAjoutbillet.setAlignment(Pos.CENTER_LEFT);
     bouttonsAjoutbillet.setHgrow(buttonValider, Priority.ALWAYS);
     bouttonsAjoutbillet.setHgrow(buttonQuitter, Priority.ALWAYS);
     buttonValider.setMaxWidth(Double.MAX_VALUE);
     buttonQuitter.setMaxWidth(Double.MAX_VALUE);
     buttonValider.setOnAction(new EventHandler<ActionEvent>() {
     	@SuppressWarnings("deprecation")
     	@Override
         public void handle(ActionEvent event) {
     		FileWriter fr = null;
     		FileWriter fr1 = null;
     		FileWriter fr2 = null;
     		FileWriter fr3 = null;
     		FileWriter fr4 = null;
     		double Montant = Double.parseDouble(readFile("montant", StandardCharsets.UTF_8));
     		double quantite_5 = Double.parseDouble(readFile("quantite_5", StandardCharsets.UTF_8));
     		double quantite_10 = Double.parseDouble(readFile("quantite_10", StandardCharsets.UTF_8));
     		double quantite_20 = Double.parseDouble(readFile("quantite_20", StandardCharsets.UTF_8));
     		double quantite_50 = Double.parseDouble(readFile("quantite_50", StandardCharsets.UTF_8));
         	try {
         	    	fr = new FileWriter(new File("montant"), false);
         	        fr.write(Double.toString(Montant+5*Double.parseDouble(cb1.getSelectionModel().getSelectedItem().toString())+10*Double.parseDouble(cb2.getSelectionModel().getSelectedItem().toString())+20*Double.parseDouble(cb3.getSelectionModel().getSelectedItem().toString())+50*Double.parseDouble(cb4.getSelectionModel().getSelectedItem().toString())));
         	        fr.write("\r\n");
         	    	fr1 = new FileWriter(new File("quantite_5"), false);
         	        fr1.write(Double.toString(quantite_5+Integer.parseInt(cb1.getSelectionModel().getSelectedItem().toString())));
         	    	fr2 = new FileWriter(new File("quantite_10"), false);
         	    	fr2.write(Double.toString(quantite_10+Integer.parseInt(cb2.getSelectionModel().getSelectedItem().toString())));
         	    	fr3 = new FileWriter(new File("quantite_20"), false);
         	    	fr3.write(Double.toString(quantite_20+Integer.parseInt(cb3.getSelectionModel().getSelectedItem().toString())));
         	    	fr4 = new FileWriter(new File("quantite_50"), false);
         	    	fr4.write(Double.toString(quantite_50+Integer.parseInt(cb4.getSelectionModel().getSelectedItem().toString())));

         	} catch (Exception e) {
         	  System.out.println("Error al escribir");
         	} finally {
             	try {
						fr.close();
						fr1.close();
						fr2.close();
						fr3.close();
						fr4.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
         	}
         	 montant.setText("Le montant dans la tirelire est :"+ readFile("montant", StandardCharsets.UTF_8));

              borderpane.setTop(hboxTop);
              borderpane.setCenter(montant);
              borderpane.setBottom(hboxBottom);
              
              scene.setRoot(borderpane);
     		
     	}
		});
	     buttonQuitter.setOnAction(new EventHandler<ActionEvent>() {
	     	@SuppressWarnings("deprecation")
	     	@Override
	         public void handle(ActionEvent event) {
	     	 montant.setText("Le montant dans la tirelire est :"+ readFile("montant", StandardCharsets.UTF_8));
	          borderpane.setTop(hboxTop);
	          borderpane.setCenter(montant);
	          borderpane.setBottom(hboxBottom);
	          
	          scene.setRoot(borderpane);
	     	}
	     });
     
        myButton.addTrigger(new GpioCallbackTrigger(new Callable<Void>() {
            public Void call() throws Exception {
            	Platform.runLater(() -> {
	                try {
	                    borderpane.setCenter(null);
	               	 	borderpane.setBottom(null);
	               	 	borderpane.setTop(null);
	               	 	
		               
		                 
	                    borderpane.setBottom(bouttonsAjoutbillet);
		                 borderpane.setCenter(vboxAjoutBillets);
	               	 	 scene.setRoot(borderpane);
	                }catch(Exception n) {
	                	n.printStackTrace();
	                }
            	});
                return null;
            }
        }));

>>>>>>> c4206eb7138a6b5cec416eb3af092c0c1a25203f
        vbox.setAlignment(Pos.CENTER);
        borderpane.setCenter(vbox);
        
        borderpane.setPadding(new Insets(0, 0, 5, 0));
        
        hbox.getChildren().addAll(button1, button2, button3);
        hbox.setAlignment(Pos.CENTER_LEFT);
        
        //borderpane.setTop(connect);
        borderpane.setBottom(hbox);

        primaryStage.setFullScreen(true);
        this.scene = new Scene(borderpane, 320, 240);
        primaryStage.setScene(scene);
        primaryStage.show();

    }
	static String readFile(String path, Charset encoding) 
	{
	  byte[] encoded = null;
	try {
		encoded = Files.readAllBytes(Paths.get(path));
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  return new String(encoded, encoding);
	}
    public static void main(String[] args) {
        Application.launch(args);
    }
    
    public String checkInternetConnection()
    {
         String status = "";
         Socket sock = new Socket();
         InetSocketAddress address = new InetSocketAddress("www.google.com", 80);

         try
         {
            sock.connect(address, 3000);
            if(sock.isConnected()) status = "Connect� au r�seau, l�IP : " + sock.getLocalAddress().getHostAddress();
         }
         catch(Exception e)
         {
             status = "Non connect� au r�seau";  
         }
         finally
         {
            try
             {
                sock.close();
             }
             catch(Exception e){}
         }

         return status;
    }

    
    
    private String pad(int fieldWidth, char padChar, String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = s.length(); i < fieldWidth; i++) {
          sb.append(padChar);
        }
        sb.append(s);

        return sb.toString();
      }

      static String getResource(String path) {
        return Clock.class.getResource(path).toExternalForm();
      }
      
      // records relative x and y co-ordinates.
      class Delta { double x, y; }  
      
      
      public Parent createLayout() {
    	    // construct the analogueClock pieces.
    	    final Circle face = new Circle(100, 100, 45);
    	    face.setId("face");
    	    final Line hourHand = new Line(0, 0, 0, -20);
    	    hourHand.setTranslateX(100);
    	    hourHand.setTranslateY(100);
    	    hourHand.setId("hourHand");
    	    final Line minuteHand = new Line(0, 0, 0, -30);
    	    minuteHand.setTranslateX(100);
    	    minuteHand.setTranslateY(100);
    	    minuteHand.setId("minuteHand");
    	    final Line secondHand = new Line(0, 15, 0, -35);
    	    secondHand.setTranslateX(100);
    	    secondHand.setTranslateY(100);
    	    secondHand.setId("secondHand");
    	    final Circle spindle = new Circle(100, 100, 5);
    	    spindle.setId("spindle");
    	    Group ticks = new Group();
    	    for (int i = 0; i < 12; i++) {
    	        Line tick = new Line(0, -32, 0, -38);
    	        tick.setTranslateX(100);
    	        tick.setTranslateY(100);
    	        tick.getStyleClass().add("tick");
    	        tick.getTransforms().add(new Rotate(i * (360 / 12)));
    	        ticks.getChildren().add(tick);
    	    }
    	    final Group analogueClock = new Group(face, ticks, spindle, hourHand, minuteHand, secondHand);

    	    // determine the starting time.
    	    Calendar calendar = GregorianCalendar.getInstance();
    	    final double seedSecondDegrees = calendar.get(Calendar.SECOND) * (360 / 60);
    	    final double seedMinuteDegrees = (calendar.get(Calendar.MINUTE) + seedSecondDegrees / 360.0) * (360 / 60);
    	    final double seedHourDegrees = (calendar.get(Calendar.HOUR) + seedMinuteDegrees / 360.0) * (360 / 12);

    	    // define rotations to map the analogueClock to the current time.
    	    final Rotate hourRotate = new Rotate(seedHourDegrees);
    	    final Rotate minuteRotate = new Rotate(seedMinuteDegrees);
    	    final Rotate secondRotate = new Rotate(seedSecondDegrees);
    	    hourHand.getTransforms().add(hourRotate);
    	    minuteHand.getTransforms().add(minuteRotate);
    	    secondHand.getTransforms().add(secondRotate);

    	    // the hour hand rotates twice a day.
    	    final Timeline hourTime = new Timeline(
    	            new KeyFrame(
    	                    Duration.hours(12),
    	                    new KeyValue(
    	                            hourRotate.angleProperty(),
    	                            360 + seedHourDegrees,
    	                            Interpolator.LINEAR
    	                    )
    	            )
    	    );

    	    // the minute hand rotates once an hour.
    	    final Timeline minuteTime = new Timeline(
    	            new KeyFrame(
    	                    Duration.minutes(60),
    	                    new KeyValue(
    	                            minuteRotate.angleProperty(),
    	                            360 + seedMinuteDegrees,
    	                            Interpolator.LINEAR
    	                    )
    	            )
    	    );

    	    // move second hand rotates once a minute.
    	    final Timeline secondTime = new Timeline(
    	            new KeyFrame(
    	                    Duration.seconds(60),
    	                    new KeyValue(
    	                            secondRotate.angleProperty(),
    	                            360 + seedSecondDegrees,
    	                            Interpolator.LINEAR
    	                    )
    	            )
    	    );

    	    // time never ends.
    	    hourTime.setCycleCount(Animation.INDEFINITE);
    	    minuteTime.setCycleCount(Animation.INDEFINITE);
    	    secondTime.setCycleCount(Animation.INDEFINITE);
    	
    	    
    	    // start the analogueClock.
    	    secondTime.play();
    	    minuteTime.play();
    	    hourTime.play();



    	    // add a glow effect whenever the mouse is positioned over the clock.
    	    final Glow glow = new Glow();
    	    analogueClock.setOnMouseEntered(new EventHandler<MouseEvent>() {
    	        @Override
    	        public void handle(MouseEvent mouseEvent) {
    	            analogueClock.setEffect(glow);
    	        }
    	    });
    	    analogueClock.setOnMouseExited(new EventHandler<MouseEvent>() {
    	        @Override
    	        public void handle(MouseEvent mouseEvent) {
    	            analogueClock.setEffect(null);
    	        }
    	    });


    	    return analogueClock;
    	}
      
}

