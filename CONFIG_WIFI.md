Pour configurer notre réseau Wi-Fi, nous allons faire appel aux outils wpa_cli, wpa_supplicant.


Les étapes d'une connexion avec succés sont:

    ==> Editer le fichier /etc/wpa_supplicant/wpa_supplicant.conf et mettre la dedans: 
    
        ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
        update_config=1
        country=FR #facultatif

        network={
        	ssid="Son  SSID"
        	psk="Son clé de securité WPA/WPA2"
        	key_mgmt=WPA-PSK
        }
        
    Si le réseau ne contient pas de clé de sécurité:
    
        ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
        update_config=1
        country=FR #facultatif

        network={
        	ssid="Son  SSID"
        	key_mgmt=NONE
        }
        
    
    ==> Tuer le réseau nommé wlan0 (lié au composant de la Wi-Fi avec cette ligne:
    
        sudo killall wpa_supplicant
        
    ==> Reconfigurer le réseau avec la ligne:
            
        sudo wpa_supplicant -B -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant.conf

    ==> Finnalement on se connecte au réseau via la ligne suivante:
    
        sudo wpa_cli -i wlan0 reconfigure (Return status OK/FAIL)
    