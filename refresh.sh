#!/bin/bash

sudo chmod +777 /etc/wpa_supplicant/wpa_supplicant.conf

sudo rm -rf /var/run/wpa_supplicant/wlan0

sudo killall wpa_supplicant

sudo wpa_supplicant -B -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant.conf

sudo wpa_cli -i wlan0 reconfigure
